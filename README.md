# AngularLumen

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

## Installation

Setting up your development environment on your local machine :
```sh
$ git clone <repository-url> this repository
```
```sh
$ cd to repository directory
```
```sh
$ npm i
```
Change `environments baseUrl to your Rest Api url`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or don't hesitate call me +37455399666
