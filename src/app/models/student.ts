export interface Student {
    id: number;
    name: string;
    last_name: string;
    phone: string;
    photo: string;
    faculty_id: number;
    group_id: number;
    course_id: number;
}
