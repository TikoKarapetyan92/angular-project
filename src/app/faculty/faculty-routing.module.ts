import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FacultyComponent} from './faculty.component';
import {AddComponent} from './component/add/add.component';
import {EditComponent} from './component/edit/edit.component';
import {ListComponent} from './component/list/list.component';

const routes: Routes = [
    {
        path: '', component: FacultyComponent, pathMatch: 'prefix',
        children: [
            {
                path: 'add', component: AddComponent
            },
            {
                path: 'edit/:id', component: EditComponent
            },
            {
                path: 'list', component: ListComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacultyRoutingModule { }
