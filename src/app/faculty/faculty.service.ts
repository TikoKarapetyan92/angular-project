import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Faculty} from '../models/faculty';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
    providedIn: 'root'
})
export class FacultyService {

    baseUrl = environment.baseUrl;
    version = environment.version;

    constructor(private http: HttpClient) { }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }
    getFaculties (): Observable<Faculty[]> {
        const url = `${this.baseUrl}${this.version}faculty`;
        return this.http.get<Faculty[]>(url).pipe(
            tap(_ => console.log(`get Faculties`)),
            catchError(this.handleError<Faculty[]>(`getFaculties`))
        );
    }

    getFaculty (id: number): Observable<Faculty[]> {
        const url = `${this.baseUrl}${this.version}faculty/${id}`;
        return this.http.get<Faculty[]>(url).pipe(
            tap(_ => console.log(`fetched faculty id=${id}`)),
            catchError(this.handleError<Faculty[]>(`getFaculty id=${id}`))
        );
    }

    addFaculty (faculty): Observable<Faculty[]> {
        const url = `${this.baseUrl}${this.version}faculty`;
        return this.http.post<Faculty[]>(url, faculty, httpOptions).pipe(
            tap(_ => console.log(`added faculty w/ id=${faculty.name}`)),
            catchError(this.handleError<Faculty[]>('addCourse'))
        );
    }

    updateFaculty (id, faculty): Observable<Faculty[]> {
        const url = `${this.baseUrl}${this.version}faculty/${id}`;
        return this.http.put(url, faculty, httpOptions).pipe(
            tap(_ => console.log(`updated faculty id=${id}`)),
            catchError(this.handleError<any>('updateFaculty'))
        );
    }

    deleteFaculty (id): Observable<Faculty[]> {
        const url = `${this.baseUrl}${this.version}faculty/${id}`;

        return this.http.delete<Faculty[]>(url, httpOptions).pipe(
            tap(_ => console.log(`deleted faculty id=${id}`)),
            catchError(this.handleError<Faculty[]>('deleteFaculty'))
        );
    }
}
