import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Faculty} from '../../../models/faculty';
import {FacultyService} from '../../faculty.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    faculties: Faculty[] = [];

    constructor(private service: FacultyService,
                private router: Router) { }

    ngOnInit() {
        this.service.getFaculties ()
            .subscribe(res => {
                this.faculties = res;
            }, err => {
                console.log(err);
            });
    }

    deleteFaculty(id) {
        this.service.deleteFaculty(id).subscribe(res => {
            const index = this.faculties.findIndex(order => order.id === id);
            this.faculties.splice(index, 1);
        });
    }

}
