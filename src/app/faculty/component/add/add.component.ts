import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {FacultyService} from '../../faculty.service';
import {Faculty} from '../../../models/faculty';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

    course: Faculty[];
    angForm: FormGroup;

    constructor(private service: FacultyService,
                private route: ActivatedRoute,
                private router: Router,
                private fb: FormBuilder) { }

    ngOnInit() {
        this.createForm();
    }

    createForm() {
        this.angForm = this.fb.group({
            name: ['', Validators.required ]
        });
    }

    addFaculty(form: NgForm) {
        this.service.addFaculty(form)
            .subscribe(res => {
                    this.router.navigate(['/faculty/list']);
                }, (err) => {
                    console.log(err);
                }
            );
    }
}
