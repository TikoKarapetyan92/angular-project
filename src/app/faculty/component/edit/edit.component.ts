import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Faculty} from '../../../models/faculty';
import {FacultyService} from '../../faculty.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

    _id: number;
    faculty: Faculty[];
    angForm: FormGroup;

    constructor(private service: FacultyService,
                private route: ActivatedRoute,
                private router: Router,
                private fb: FormBuilder) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this._id = params['id'];
            this.service.getFaculty(params['id']).subscribe(res => {
                this.faculty = res;
                this.setValue(this.faculty);
            });
        });

        this.createForm();
    }

    createForm() {
        this.angForm = this.fb.group({
            name: ['', Validators.required ]
        });
    }

    updateFaculty (form: NgForm) {
        this.service.updateFaculty(this._id, form)
            .subscribe(res => {
                    this.router.navigate(['/faculty/list']);
                }, (err) => {
                    console.log(err);
                }
            );
    }

    setValue(data) { this.angForm.setValue({name: data.name}); }

}
