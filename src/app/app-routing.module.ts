import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';


const routes: Routes = [
    {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    }, {
        path: 'group',
        loadChildren: './group/group.module#GroupModule',
    }, {
        path: 'student',
        loadChildren: './student/student.module#StudentModule',
    } , {
        path: 'course',
        loadChildren: './course/course.module#CourseModule',
    }, {
        path: 'faculty',
        loadChildren: './faculty/faculty.module#FacultyModule',
    },
    {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
    {path: '**', redirectTo: ''},
];

const config: ExtraOptions = {
    useHash: false,
};

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule],
})
export class AppRoutingModule { }