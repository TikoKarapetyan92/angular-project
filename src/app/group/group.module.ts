import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { GroupRoutingModule } from './group-routing.module';
import { AddComponent } from './component/add/add.component';
import { EditComponent } from './component/edit/edit.component';
import { GroupComponent } from './group.component';
import { ListComponent } from './component/list/list.component';

@NgModule({
  declarations: [
      AddComponent,
      EditComponent,
      GroupComponent,
      ListComponent
  ],
  imports: [
    CommonModule,
    GroupRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class GroupModule { }
