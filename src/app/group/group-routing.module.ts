import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GroupComponent} from './group.component';
import {AddComponent} from './component/add/add.component';
import {EditComponent} from './component/edit/edit.component';
import {ListComponent} from './component/list/list.component';

const routes: Routes = [
    {
        path: '', component: GroupComponent, pathMatch: 'prefix',
        children: [
            {
                path: 'add', component: AddComponent
            },
            {
                path: 'edit/:id', component: EditComponent
            },
            {
                path: 'list', component: ListComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupRoutingModule { }
