import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {Group} from '../models/group';
import {environment} from '../../environments/environment';
import {Faculty} from '../models/faculty';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
    providedIn: 'root'
})
export class GroupService {

    baseUrl = environment.baseUrl;
    version = environment.version;

    constructor(private http: HttpClient) {
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }

    getFaculties(): Observable<Faculty[]> {
        const url = `${this.baseUrl}${this.version}faculty`;
        return this.http.get<Faculty[]>(url).pipe(
            tap(_ => console.log(`get Faculties`)),
            catchError(this.handleError<Faculty[]>(`getFaculties`))
        );
    }

    getGroups(): Observable<Group[]> {
        const url = `${this.baseUrl}${this.version}group`;
        return this.http.get<Group[]>(url).pipe(
            tap(_ => console.log(`get Groups`)),
            catchError(this.handleError<Group[]>(`getGroups`))
        );
    }

    getGroup(id: number): Observable<Group[]> {
        const url = `${this.baseUrl}${this.version}group/${id}`;
        return this.http.get<Group[]>(url).pipe(
            tap(_ => console.log(`fetched Group id=${id}`)),
            catchError(this.handleError<Group[]>(`getGroup id=${id}`))
        );
    }

    addGroup(group): Observable<Group[]> {
        const url = `${this.baseUrl}${this.version}group`;
        return this.http.post<Group[]>(url, group, httpOptions).pipe(
            tap(_ => console.log(`added group w/ name=${name}`)),
            catchError(this.handleError<Group[]>('addGroup'))
        );
    }

    updateGroup(id, group): Observable<Group[]> {
        const url = `${this.baseUrl}${this.version}group/${id}`;
        return this.http.put<Group[]>(url, group, httpOptions).pipe(
            tap(_ => console.log(`updated group id=${id}`)),
            catchError(this.handleError<Group[]>('updateGroup'))
        );
    }

    deleteGroup(id): Observable<Group[]> {
        const url = `${this.baseUrl}${this.version}group/${id}`;
        return this.http.delete<Group[]>(url, httpOptions).pipe(
            tap(_ => console.log(`deleted group id=${id}`)),
            catchError(this.handleError<Group[]>('deleteGroup'))
        );
    }
}
