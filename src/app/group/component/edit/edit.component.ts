import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Group} from '../../../models/group';
import {GroupService} from '../../group.service';
import {Faculty} from '../../../models/faculty';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

    _id: number;
    group: Group[];
    faculties: Faculty[];
    angForm: FormGroup;

    constructor(private service: GroupService,
                private route: ActivatedRoute,
                private router: Router,
                private fb: FormBuilder) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this._id = params['id'];
            this.service.getGroup(params['id']).subscribe(res => {
                this.group = res;
                this.setValue(this.group);
            });
        });

        this.createForm();
        this.getFaculties();
    }

    createForm() {
        this.angForm = this.fb.group({
            name: ['', Validators.required ],
            faculty_id: ['', Validators.required]
        });
    }

    getFaculties() {
        this.service.getFaculties ()
            .subscribe(res => {
                console.log(res);
                this.faculties = res;
            }, err => {
                console.log(err);
            });
    }

    updateGroup (form: NgForm) {
        this.service.updateGroup(this._id, form)
            .subscribe(res => {
                    this.router.navigate(['/group/list']);
                }, (err) => {
                    console.log(err);
                }
            );
    }

    setValue(data) { this.angForm.setValue({name: data.name, faculty_id: data.faculty_id }); }

}
