import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {GroupService} from '../../group.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Group} from '../../../models/group';
import {Faculty} from '../../../models/faculty';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

    group: Group[];
    faculties: Faculty[];
    angForm: FormGroup;

    constructor(private service: GroupService,
                private route: ActivatedRoute,
                private router: Router,
                private fb: FormBuilder) { }

  ngOnInit() {
      this.createForm();
      this.getFaculties();
  }

    createForm() {
        this.angForm = this.fb.group({
            name: ['', Validators.required ],
            faculty_id: ['', Validators.required]
        });
    }

    getFaculties() {
        this.service.getFaculties ()
            .subscribe(res => {
                console.log(res);
                this.faculties = res;
            }, err => {
                console.log(err);
            });
    }

    addGroup(form: NgForm) {
        this.service.addGroup(form)
            .subscribe(res => {
                    this.router.navigate(['/group/list']);
                }, (err) => {
                    console.log(err);
                }
            );
    }
}
