import {Component, OnInit} from '@angular/core';
import {GroupService} from '../../group.service';
import {Group} from '../../../models/group';
import {Faculty} from '../../../models/faculty';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    groups: Group[];
    faculties: Faculty[];

    constructor(private service: GroupService) {
    }

    ngOnInit() {
        this.getGroups();
    }

    getGroups() {
        this.service.getGroups()
            .subscribe(res => {
                console.log(res);
                this.groups = res;
            }, err => {
                console.log(err);
            });
    }

    deleteGroup(id) {
        this.service.deleteGroup(id).subscribe(res => {
            const index = this.groups.findIndex(order => order.id === id);
            this.groups.splice(index, 1);
        });
    }

}
