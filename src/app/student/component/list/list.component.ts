import { Component, OnInit } from '@angular/core';
import {Faculty} from '../../../models/faculty';
import {Student} from '../../../models/student';
import {StudentService} from '../../student.service';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    students: Student[];
    faculties: Faculty[];
    baseUrl = environment.baseUrl;

    constructor(private service: StudentService) {
    }

    ngOnInit() {
        this.getStudents();
    }

    getStudents() {
        this.service.getStudents()
            .subscribe(res => {
                this.students = res;
            }, err => {
                console.error(err);
            });
    }

    deleteStudent(id) {
        this.service.deleteStudent(id).subscribe(res => {
            const index = this.students.findIndex(order => order.id === id);
            this.students.splice(index, 1);
        });
    }

}
