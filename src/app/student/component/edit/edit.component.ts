import { Component, OnInit } from '@angular/core';
import {Student} from '../../../models/student';
import {Faculty} from '../../../models/faculty';
import {Course} from '../../../models/course';
import {Group} from '../../../models/group';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {StudentService} from '../../student.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

    student: Student[];
    faculties: Faculty[];
    courses: Course[];
    groups: Group[];
    angForm: FormGroup;
    _id: number;

    constructor(private service: StudentService,
                private route: ActivatedRoute,
                private router: Router,
                private fb: FormBuilder
    ) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this._id = params['id'];
            this.service.getStudent(params['id']).subscribe(res => {
                this.student = res;
                this.setValue(this.student);
            });
        });

        this.createForm();
        this.getFaculties();
        this.getCourses();
        this.getGroups();
    }

    createForm() {
        this.angForm = this.fb.group({
            name: ['', Validators.required],
            last_name: ['', Validators.required],
            phone: ['', Validators.required],
            photo: ['', null],
            faculty_id: ['', Validators.required],
            course_id: ['', Validators.required],
            group_id: ['', Validators.required],
        });
    }

    getFaculties() {
        this.service.getFaculties()
            .subscribe(res => {
                this.faculties = res;
            }, err => {
                console.log(err);
            });
    }

    getGroups() {
        this.service.getGroups()
            .subscribe(res => {
                this.groups = res;
            }, err => {
                console.log(err);
            });
    }

    getCourses() {
        this.service.getCourses()
            .subscribe(res => {
                this.courses = res;
            }, err => {
                console.log(err);
            });
    }

    updateStudent(form: NgForm) {
        const formData = new FormData();
        formData.append('name', this.angForm.get('name').value);
        formData.append('last_name', this.angForm.get('last_name').value);
        formData.append('phone', this.angForm.get('phone').value);
        formData.append('photo', this.angForm.get('photo').value);
        formData.append('faculty_id', this.angForm.get('faculty_id').value);
        formData.append('group_id', this.angForm.get('group_id').value);
        formData.append('course_id', this.angForm.get('course_id').value);
        this.service.updateStudent(this._id, formData)
            .subscribe(res => {
                    this.router.navigate(['/student/list']);
                }, (err) => {
                    console.log(err);
                }
            );
    }

    uploadFile(event) {
        const elem = event.target;
        if (elem.files.length > 0) {
            this.angForm.get('photo').setValue(elem.files[0]);
        }

    }


    setValue(data) { this.angForm.setValue({
        name: data.name,
        faculty_id: data.faculty_id,
        last_name: data.last_name,
        phone: data.phone,
        photo: data.photo,
        group_id: data.group_id,
        course_id: data.course_id
    }); }


}
