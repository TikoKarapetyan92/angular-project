import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {Group} from '../../../models/group';
import {Faculty} from '../../../models/faculty';
import {ActivatedRoute, Router} from '@angular/router';
import {Student} from '../../../models/student';
import {StudentService} from '../../student.service';
import {Course} from '../../../models/course';


@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

    student: Student[];
    faculties: Faculty[];
    courses: Course[];
    groups: Group[];
    angForm: FormGroup;

    constructor(private service: StudentService,
                private route: ActivatedRoute,
                private router: Router,
                private fb: FormBuilder
    ) {
    }

    ngOnInit() {
        this.createForm();
        this.getFaculties();
        this.getCourses();
        this.getGroups();
    }

    createForm() {
        this.angForm = this.fb.group({
            name: ['', Validators.required],
            last_name: ['', Validators.required],
            phone: ['', Validators.required],
            photo: ['', null],
            faculty_id: ['', Validators.required],
            course_id: ['', Validators.required],
            group_id: ['', Validators.required],
        });
    }

    getFaculties() {
        this.service.getFaculties()
            .subscribe(res => {
                this.faculties = res;
            }, err => {
                console.log(err);
            });
    }

    getGroups() {
        this.service.getGroups()
            .subscribe(res => {
                this.groups = res;
            }, err => {
                console.log(err);
            });
    }

    getCourses() {
        this.service.getCourses()
            .subscribe(res => {
                this.courses = res;
            }, err => {
                console.log(err);
            });
    }

    addStudent(form: NgForm) {
        const formData = new FormData();
                formData.append('name', this.angForm.get('name').value);
                formData.append('last_name', this.angForm.get('last_name').value);
                formData.append('phone', this.angForm.get('phone').value);
                formData.append('photo', this.angForm.get('photo').value);
                formData.append('faculty_id', this.angForm.get('faculty_id').value);
                formData.append('group_id', this.angForm.get('group_id').value);
                formData.append('course_id', this.angForm.get('course_id').value);
        this.service.addStudent(formData)
            .subscribe(res => {
                    this.router.navigate(['/student/list']);
                }, (err) => {
                    console.log(err);
                }
            );
    }

    uploadFile(event) {
        const elem = event.target;
        if (elem.files.length > 0) {
            this.angForm.get('photo').setValue(elem.files[0]);
        }

    }

}
