import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Faculty} from '../models/faculty';
import {Student} from '../models/student';
import {Course} from '../models/course';
import {Group} from '../models/group';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
    providedIn: 'root'
})
export class StudentService {

    baseUrl = environment.baseUrl;
    version = environment.version;

    constructor(private http: HttpClient) { }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }

    getFaculties (): Observable<Faculty[]> {
        const url = `${this.baseUrl}${this.version}faculty`;
        return this.http.get<Faculty[]>(url).pipe(
            tap(_ => console.log(`get Faculties`)),
            catchError(this.handleError<Faculty[]>(`getFaculties`))
        );
    }

    getCourses (): Observable<Course[]> {
        const url = `${this.baseUrl}${this.version}course`;
        return this.http.get<Course[]>(url).pipe(
            tap(_ => console.log(`get Courses`)),
            catchError(this.handleError<Course[]>(`getCourses`))
        );
    }

    getGroups(): Observable<Group[]> {
        const url = `${this.baseUrl}${this.version}group`;
        return this.http.get<Group[]>(url).pipe(
            tap(_ => console.log(`get Groups`)),
            catchError(this.handleError<Group[]>(`getGroups`))
        );
    }

    getStudents (): Observable<Student[]> {
        const url = `${this.baseUrl}${this.version}student`;
        return this.http.get<Student[]>(url).pipe(
            tap(_ => console.log(`get Students`)),
            catchError(this.handleError<Student[]>(`getStudents`))
        );
    }

    getStudent (id: number): Observable<Student[]> {
        const url = `${this.baseUrl}${this.version}student/${id}`;
        return this.http.get<Student[]>(url).pipe(
            tap(_ => console.log(`fetched Student id=${id}`)),
            catchError(this.handleError<Student[]>(`getStudent id=${id}`))
        );
    }

    addStudent (student): Observable<Student[]> {
        const url = `${this.baseUrl}${this.version}student`;
        console.log(student);
        return this.http.post<Student[]>(url, student).pipe(
            tap(_ => console.log(_)),
            catchError(this.handleError<Student[]>('addGroup'))
        );
    }

    updateStudent (id, student): Observable<Student> {
        const url = `${this.baseUrl}${this.version}student/${id}`;
        return this.http.post<Student>(url, student).pipe(
            tap(_ => console.log(`updated student id=${id}`)),
            catchError(this.handleError<Student>('updateStudent'))
        );
    }

    deleteStudent (id): Observable<Student[]> {
        const url = `${this.baseUrl}${this.version}student/${id}`;
        return this.http.delete<Student[]>(url, httpOptions).pipe(
            tap(_ => console.log(`deleted student id=${id}`)),
            catchError(this.handleError<Student[]>('deleteStudent'))
        );
    }
}
