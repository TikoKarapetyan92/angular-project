import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { CourseRoutingModule } from './course-routing.module';
import { CourseComponent } from './course.component';
import { AddComponent } from './component/add/add.component';
import { EditComponent } from './component/edit/edit.component';
import { ListComponent } from './component/list/list.component';

@NgModule({
  declarations: [CourseComponent, AddComponent, EditComponent, ListComponent],
  imports: [
      CommonModule,
      CourseRoutingModule,
      FormsModule,
      ReactiveFormsModule
  ]
})
export class CourseModule { }
