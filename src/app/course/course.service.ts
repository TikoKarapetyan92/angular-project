import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Course} from '../models/course';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class CourseService {

    baseUrl = environment.baseUrl;
    version = environment.version;

    constructor(private http: HttpClient) { }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }
    getCourses (): Observable<Course[]> {
        const url = `${this.baseUrl}${this.version}course`;
        return this.http.get<Course[]>(url).pipe(
            tap(_ => console.log(`get all course`)),
            catchError(this.handleError<Course[]>(`getCourses`))
        );
    }

    getCourse (id: number): Observable<Course[]> {
        const url = `${this.baseUrl}${this.version}course/${id}`;
        return this.http.get<Course[]>(url).pipe(
            tap(_ => console.log(`fetched course id=${id}`)),
            catchError(this.handleError<Course[]>(`getCourse id=${id}`))
        );
    }

    addCourse (course): Observable<Course[]> {
        const url = `${this.baseUrl}${this.version}course`;
        return this.http.post<Course[]>(url, course, httpOptions).pipe(
            tap(_ => console.log(`added course w/ name=${course.name}`)),
            catchError(this.handleError<Course[]>('addCourse'))
        );
    }

    updateCourse (id, course): Observable<Course[]> {
        const url = `${this.baseUrl}${this.version}course/${id}`;
        return this.http.put(url, course, httpOptions).pipe(
            tap(_ => console.log(`updated course id=${id}`)),
            catchError(this.handleError<any>('updateCourse'))
        );
    }

    deleteCourse (id): Observable<Course[]> {
        const url = `${this.baseUrl}${this.version}course/${id}`;

        return this.http.delete<Course[]>(url, httpOptions).pipe(
            tap(_ => console.log(`deleted course id=${id}`)),
            catchError(this.handleError<Course[]>('deleteCourse'))
        );
    }
}
