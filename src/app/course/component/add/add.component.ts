import { Component, OnInit } from '@angular/core';
import {Course} from '../../../models/course';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {CourseService} from '../../course.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  course: Course[];
  angForm: FormGroup;

  constructor(private service: CourseService,
              private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder) { }

  ngOnInit() {
      this.createForm();
  }

  createForm() {
      this.angForm = this.fb.group({
          name: ['', Validators.required ]
      });
  }

  addCourse(form: NgForm) {
      this.service.addCourse(form)
          .subscribe(res => {
                  this.router.navigate(['/course/list']);
              }, (err) => {
                  console.log(err);
              }
          );
  }
}
