import { Component, OnInit } from '@angular/core';
import {CourseService} from '../../course.service';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Course} from '../../../models/course';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {

  _id: number;
  course: Course[];
  angForm: FormGroup;

  constructor(private service: CourseService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder) { }

  ngOnInit() {
      this.route.params.subscribe(params => {
          this._id = params['id'];
          this.service.getCourse(params['id']).subscribe(res => {
              this.course = res;
              this.setValue(this.course);
          });
      });

      this.createForm();
  }

    createForm() {
        this.angForm = this.fb.group({
            name: ['', Validators.required ]
        });
    }

    updateCourse(form: NgForm) {
        this.service.updateCourse(this._id, form)
            .subscribe(res => {
                    this.router.navigate(['/course/list']);
                }, (err) => {
                    console.log(err);
                }
            );
    }

    setValue(data) { this.angForm.setValue({name: data.name}); }

}
