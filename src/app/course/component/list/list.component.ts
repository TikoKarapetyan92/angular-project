import { Component, OnInit } from '@angular/core';
import {CourseService} from '../../course.service';
import {Course} from '../../../models/course';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    courses: Course[] = [];

  constructor(private service: CourseService,
              private router: Router) { }

  ngOnInit() {
      this.service.getCourses ()
          .subscribe(res => {
              this.courses = res;
          }, err => {
              console.log(err);
          });
  }

    deleteCourse(id) {
        this.service.deleteCourse(id).subscribe(res => {
            const index = this.courses.findIndex(order => order.id === id);
            this.courses.splice(index, 1);
        });
    }

}
