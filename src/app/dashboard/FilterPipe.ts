import {Injectable, Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filter'
})
@Injectable()
export class FilterPipe implements PipeTransform {
    transform(value: any[], input: string) {
        if (input) {
            input = input.toLowerCase();
            return value.filter(function (item: any) {
                if (item.name && item.course && item.last_name && item.phone && item.faculty.name && item.group.name) {
                    return item.name.toLowerCase().includes(input) ||
                        item.last_name.toLowerCase().includes(input) ||
                        item.phone.toLowerCase().includes(input) ||
                        item.course.name.toLowerCase().includes(input) ||
                        item.faculty.name.toLowerCase().includes(input) ||
                        item.group.name.toLowerCase().includes(input);
                }
            });
        }
        return value;
    }
}
