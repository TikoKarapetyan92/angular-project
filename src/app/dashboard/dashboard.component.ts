import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { Student } from '../models/student';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    filterQuery: string;
    students: Student[];
    baseUrl = environment.baseUrl;

    constructor(private service: DashboardService) { }

  ngOnInit() {
      this.getStudents();
  }

    getStudents() {
        this.service.getStudents()
            .subscribe(res => {
                this.students = res;
            }, err => {
                console.error(err);
            });
    }
}
