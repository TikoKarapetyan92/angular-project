import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import {Student} from '../models/student';
import {environment} from '../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

    baseUrl = environment.baseUrl;
    version = environment.version;

  constructor(private http: HttpClient) { }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }

    getStudents (): Observable<Student[]> {
        const url = `${this.baseUrl}${this.version}dashboard`;
        return this.http.get<Student[]>(url).pipe(
            tap(_ => console.log(`get Students`)),
            catchError(this.handleError<Student[]>(`getStudents`))
        );
    }
}
